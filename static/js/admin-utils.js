var criticalOpenInProgressDialog = document.getElementById("critical-open-in-progress-dialog");
var criticalOpenSuccessDialog = document.getElementById("critical-open-success-dialog");
var criticalOpenFailedDialog = document.getElementById("critical-open-failed-dialog");

function openBarrier() {
    if (typeof criticalOpenInProgressDialog.showModal === "function") {
        criticalOpenInProgressDialog.showModal();
    } else {
        console.log("Navigateur non compatible");
    }

    fetch('/critical/open', {
        method: 'GET'
    }).then((response) => {
        try {
            criticalOpenInProgressDialog.close();            
        } catch (error) {
            
        }
        if (response.ok) {
            if (typeof criticalOpenSuccessDialog.showModal === "function") {
                criticalOpenSuccessDialog.showModal();
            } else {
                console.log("Navigateur non compatible");
            }
        } else {
            if (typeof criticalOpenFailedDialog.showModal === "function") {
                criticalOpenFailedDialog.showModal();
            } else {
                console.log("Navigateur non compatible");
            }
        }
    })
}

Array.from(document.getElementsByClassName('critical-open-btn')).forEach((elmt) => elmt.addEventListener('click', (e) => openBarrier()));