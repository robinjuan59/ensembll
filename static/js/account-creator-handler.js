var inputPswd = document.getElementById("input-pswd");
var inputPswdMatch = document.getElementById("input-repeat-pswd");
var warnPswdCriteria = document.getElementById("pswd-criteria");
var warnPswdMatch = document.getElementById("pswd-match");
var profilesSelector = document.getElementById("profile-type-select");

var inputMail = document.getElementById("mail");
var inputTel = document.getElementById("phonenumber");
var inputLogin = document.getElementById("login");
var warnMail = document.getElementById("warn-mail");
var warnTel = document.getElementById("warn-phonenumber");
var warnLogin = document.getElementById("warn-login");

function checkPassword(password) {
    // Password must have lower and upper case with at least one number and one special character
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/.test(password);
}

function matchPassword() {
    return inputPswd.value === inputPswdMatch.value;
}

inputPswd.addEventListener('input', (event) => {
    if (warnPswdCriteria.classList.contains("hidden") && !checkPassword(event.target.value)) {
        // Password was good before but isn't now
        warnPswdCriteria.classList.remove("hidden");
        inputPswd.classList.add('error-input');
    } else if (!warnPswdCriteria.classList.contains("hidden") && checkPassword(event.target.value)) {
        // Password wasn't good but is now
        warnPswdCriteria.classList.add("hidden");
        inputPswd.classList.remove('error-input');
    }

    // Check if password match each time we change this input too
    if (warnPswdMatch.classList.contains('hidden') && !matchPassword()) {
        warnPswdMatch.classList.remove('hidden');
        inputPswdMatch.classList.add('error-input');
    } else if (!warnPswdMatch.classList.contains('hidden') && matchPassword()) {
        warnPswdMatch.classList.add('hidden');
        inputPswdMatch.classList.remove('error-input');
    }
});

inputPswdMatch.addEventListener('input', (event) => {
    if (warnPswdMatch.classList.contains('hidden') && !matchPassword()) {
        warnPswdMatch.classList.remove('hidden');
        inputPswdMatch.classList.add('error-input');
    } else if (!warnPswdMatch.classList.contains('hidden') && matchPassword()) {
        warnPswdMatch.classList.add('hidden');
        inputPswdMatch.classList.remove('error-input');
    }
});

document.getElementById('create-form').addEventListener("submit", (event) => {
    event.preventDefault();
    inputMail.style.borderColor = "#0071ba";
    inputTel.style.borderColor = "#0071ba";
    inputLogin.style.borderColor = "#0071ba";
    warnMail.classList.add("hidden");
    warnLogin.classList.add("hidden");
    warnTel.classList.add("hidden");


    if (checkPassword(inputPswd.value) && matchPassword()) {
        // Request to server
        const formData = new FormData(event.target);

        fetch(event.target.action, {
            method: "POST",
            body: formData
        }).then((response) => {
            return response.text();
        }).then((text) => {
            switch (text) {
                case "MAIL ALREADY BOUND":
                    inputMail.style.borderColor = "red";
                    warnMail.classList.remove("hidden");
                    break;
            
                case "INCORRECT TEL":
                    inputTel.style.borderColor = "red";
                    warnTel.classList.remove("hidden");
                    break;

                case "LOGIN ALREADY EXISTS":
                    inputLogin.style.borderColor = "red";
                    warnLogin.classList.remove("hidden");
                    break;

                default:
                    document.documentElement.innerHTML = text;
                    break;
            }
        });
    }
});

function clearSelector(select) {
    let l = select.options.length;
    for (let i = 0; i < l; i++) {
        select.remove(0);
    }
}

function fetchProfilesByEntity(entityId) {
    fetch('/profiles?' + new URLSearchParams({'entity': entityId})).then((response) => {
        return response.json();
    }).then((data) => {
        for (let p of data) {
            let o = document.createElement('option');
            o.text = p;
            profilesSelector.add(o);
        }
    });
}

document.getElementById('entity-select').addEventListener('change', (e) => {
    clearSelector(profilesSelector);
    fetchProfilesByEntity(e.target.value);
});

fetchProfilesByEntity(document.getElementById('entity-select').value);